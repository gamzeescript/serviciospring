package com.ampersand.restfull.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/** Entidad de la cual crearemos la persistencia y de donde se consumiran nuestros datos */

@Entity
@Table(name="aficion")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Aficion {
	
	private long aficion_id;
	private String nombre;
	private String descripcion;
	
	public Aficion() {
		
	}


	public Aficion(long aficion_id, String nombre, String descripcion) {
		this.aficion_id = aficion_id;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="aficion_id")
	public long getAficion_id() {
		return aficion_id;
	}


	public void setAficion_id(long aficion_id) {
		this.aficion_id = aficion_id;
	}


	@Column(name="nombre")
	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	@Column(name="descripcion")
	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
