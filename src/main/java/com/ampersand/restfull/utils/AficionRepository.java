package com.ampersand.restfull.utils;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ampersand.restfull.beans.Aficion;

/**  interface que se comunicara tanto con los modulos de Spring como con el modulo
	de la persistencia (interfaz de contrato - interfaz de control) */

@Repository
public interface AficionRepository extends JpaRepository<Aficion, Long>{

}
