package com.ampersand.restfull.utils;

import java.util.List;

/* mi interface que hace referencia a mi clase abstracta - se utiliza patron Abstrac Facade */

public interface ContAficion<T> {
	
	public List <T> gets();
	
	public T getById(Long id);
	
	public T registry(T r);
	
	public void clean(T e);


}
