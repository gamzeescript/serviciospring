package com.ampersand.restfull.apis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ampersand.restfull.beans.Aficion;
import com.ampersand.restfull.utils.ContAficion;

/** Clase controladora que administra las peticiones que vendran de nuestro cliente y las procesara entre nuestros modulos */

@RestController 
@RequestMapping("/apis")
public class Hobbies {
	
	@Autowired
	ContAficion af;

	@GetMapping("/afics")
	public List<Aficion> getAll(){ 

	return af.gets();

	 }

	 @GetMapping("/afics/{id}")
	 public ResponseEntity<Aficion> getById(@PathVariable(value = "id")Long id) { 
	 	Aficion fic = (Aficion) af.getById(id);
	 	return ResponseEntity.ok().body(fic);


	 }

	 @PostMapping("/afics")
	 public Aficion store(@RequestBody Aficion afic){ 

	return (Aficion) af.registry(afic);

	  }

	  @PutMapping("/afics/{id}")
	  public ResponseEntity<Aficion> update(@PathVariable(value = "id")Long id, @RequestBody Aficion afic){

	  Aficion fig = (Aficion) af.getById(id);
	  fig.setNombre(afic.getNombre());
	  fig.setDescripcion(afic.getDescripcion());
	  final Aficion actual = (Aficion) af.registry(fig);
	  return ResponseEntity.ok(actual);


	   }

	   @DeleteMapping("/afics/{id}")
	   public Map<String, Boolean> delete(@PathVariable(value = "id")Long id){

	   Aficion quitar = (Aficion) af.getById(id);
	   af.clean(quitar);
	   Map<String, Boolean> response = new HashMap<String, Boolean>();
	   response.put("borrado exitoso", true);
	   return response;

}
	   
}