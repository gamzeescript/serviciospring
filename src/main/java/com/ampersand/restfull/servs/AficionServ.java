package com.ampersand.restfull.servs;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ampersand.restfull.utils.AficionRepository;
import com.ampersand.restfull.beans.Aficion;
import com.ampersand.restfull.utils.ContAficion;

/** Mi clase abstracta que al tener la anotacion de servicio lo convierte en un 
    servicio */

@Service 
public class AficionServ implements ContAficion<Aficion>{
	
	@Autowired
	AficionRepository afi;

	@Override
	public List<Aficion> gets() {
		
		return afi.findAll();
	}

	@Override
	public Aficion getById(Long id) {
		
		return afi.getOne(id);
	}

	@Override
	public Aficion registry(Aficion r) {

		return afi.save(r);
	}

	@Override
	public void clean(Aficion e) {
		
		afi.delete(e);
	}
	
	

}
