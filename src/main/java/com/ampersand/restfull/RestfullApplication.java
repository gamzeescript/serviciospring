package com.ampersand.restfull;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/* Metodo main para hacer correr la aplicacion  
   Anotaciones de configuracion y de nuestro Group Id que 
   es donde se ubican nuestros modulos	*/
   
@Configuration
@ComponentScan("com.ampersand")
@SpringBootApplication
public class RestfullApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestfullApplication.class, args);
	}

}
